﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckList.Models
{
    public class Check
    {
        public int CheckId { get; set; }
        public String CheckDescription { get; set; }
        public bool CheckIsDone { get; set; }
        public DateTime CheckDueDate { get; set; }
        public virtual ApplicationUser CheckUser { get; set; }
        public bool CheckIsNotified { get; set; }
    }
}