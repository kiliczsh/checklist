﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CheckList.Models;
using Microsoft.AspNet.Identity;

namespace CheckList.Controllers
{
    public class ChecksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Checks
        public ActionResult Index()
        {
            return View();
        }

        private IEnumerable<Check> GetChecks()
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(u => u.Id == currentUserId);
            IEnumerable<Check> checks = db.Checks.ToList().Where(u => u.CheckUser == currentUser).OrderBy(u => u.CheckDueDate);
            int count = 0;
            foreach(Check check in checks)
            {
                if (check.CheckIsDone)
                {
                    count++;
                }
            }

            ViewBag.Percent = Math.Round(100f * ((float)count/(float)checks.Count()));
            IEnumerable<Check> incomingChecks = checks.Where(x => x.CheckIsDone == false && !x.CheckIsNotified).Where(x => DateTime.Compare(x.CheckDueDate.AddHours(12), DateTime.Now) > 0);
            ViewData["Item"] = "";
            if (incomingChecks != null && incomingChecks.Count() >= 1)
            {
                ViewData["Item"] = incomingChecks.First().CheckDescription;
                ViewData["ItemId"] = incomingChecks.First().CheckId;
            }
            return checks;
        }

        [HttpPost]
        public ActionResult UpdateCheckNotify(int id)
        {
            Check incomingCheck = db.Checks.FirstOrDefault(c =>(c.CheckId == id));
            incomingCheck.CheckIsNotified = true;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }



        public ActionResult CreateCheckTable()
        {
            return PartialView("_CheckTable", GetChecks());
        }

        // GET: Checks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Check check = db.Checks.Find(id);
            if (check == null)
            {
                return HttpNotFound();
            }
            return View(check);
        }

        // GET: Checks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Checks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CheckId,CheckDescription,CheckIsDone,CheckDueDate")] Check check)
        {
            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == currentUserId);
                check.CheckUser = currentUser;
                check.CheckIsNotified = false;
                db.Checks.Add(check);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(check);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AJAXCreate([Bind(Include = "CheckId,CheckDescription,CheckDueDate")] Check check)
        {
            if (ModelState.IsValid)
            {
                if(check.CheckDescription == null || check.CheckDescription == "")
                {//if description is empty
                    return View(check);
                }
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == currentUserId);
                check.CheckUser = currentUser;
                check.CheckIsDone = false;
                check.CheckIsNotified = false;
                db.Checks.Add(check);
                db.SaveChanges();
            }

            return PartialView("_CheckTable", GetChecks());
        }

        // GET: Checks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Check check = db.Checks.Find(id);
            if (check == null)
            {
                return HttpNotFound();
            }
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == currentUserId);
            if(check.CheckUser != currentUser)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(check);
        }

        // POST: Checks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CheckId,CheckDescription,CheckIsDone,CheckDueDate")] Check check)
        {
            if (ModelState.IsValid)
            {
                db.Entry(check).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(check);
        }

        [HttpPost]
        public ActionResult AJAXEdit(int? id, bool value)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Check check = db.Checks.Find(id);

            if (check == null)
            {
                return HttpNotFound();
            }
            else
            {
                check.CheckIsDone = value;
                db.Entry(check).State = EntityState.Modified;
                db.SaveChanges();
                return PartialView("_CheckTable", GetChecks());
            }
        }

        // GET: Checks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Check check = db.Checks.Find(id);
            if (check == null)
            {
                return HttpNotFound();
            }
            return View(check);
        }

        // POST: Checks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Check check = db.Checks.Find(id);
            db.Checks.Remove(check);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
