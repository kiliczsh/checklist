namespace CheckList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CheckIsNotifiedAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Checks", "CheckIsNotified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Checks", "CheckIsNotified");
        }
    }
}
